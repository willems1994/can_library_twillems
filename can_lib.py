import can
import threading
import time

def send_can_message(bus, our_id, our_data, ext_id):
    msg = can.Message(arbitration_id=our_id, data=our_data, extended_id=ext_id)

    try:
        bus.send(msg)
        print("message sent on {}".format(bus.channel_info))
    except can.CanError:
        print("message NOT sent");

class CanListener(can.Listener):
    def __init__(self, callback):
        self.callback = callback

    def on_message_received(self, msg):
        self.callback(msg)
        

def init_can(new_bitrate):
    #bus = can.interface.Bus(bustype='pcan', channel='PCAN_USBBUS1', bitrate=new_bitrate)
    bus = can.interface.Bus(bustype='socketcan', channel='can0', bitrate=new_bitrate)
    return bus

def init_receive_listener(bus, can_id, can_mask, extended, callback):
    can_filters = [{"can_id": can_id,"can_mask": can_mask, "extended": extended}]
    bus.set_filters(can_filters)
    a_listener = CanListener(callback)
    notifier = can.Notifier(bus, [a_listener])
