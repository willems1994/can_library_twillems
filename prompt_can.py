#!/usr/bin/env python

import struct
import sys
import select

from can_lib import *

def receive_handler(msg):
    print("Message received with id: {} and data: {}".format(msg.arbitration_id, msg.data))

def send_message():
    b7 = 0x01
    b6 = 0x02
    b5 = 0x03
    b4 = 0x04
    b3 = 0x05
    b2 = 0x06
    b1 = 0x07
    b0 = 0x08
    send_can_message(bus, 0x01, [b0, b1, b2, b3, b4, b5, b6, b7], False)


if __name__ == "__main__":
    BITRATE = 125000
    bus = init_can(BITRATE)
    init_receive_listener(bus, 0x00, 0x00, False, receive_handler)

    while 1:
        send_message()
        time.sleep(5)
